package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SmartParkingBoyTest {
    @Test
    void should_return_parkingLot1_ticket_when_park_given_2parkingLot_parkingBoy() throws Exception {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();

        ParkingBoy parkingBoy = new ParkingBoy(Arrays.asList(parkingLot1, parkingLot2));
        parkingBoy.setParkingStrategy(new SmartParkingStrategy());
        Car car = new Car();

        //when
        Ticket actualTicket = parkingBoy.park(car);

        //then
        assertEquals(car, parkingLot1.fetch(actualTicket));
    }

    @Test
    void should_return_parkinglot2_ticket_when_park_given_lot1_full_lot2_available() throws Exception {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(Arrays.asList(parkingLot1, parkingLot2));
        parkingBoy.setParkingStrategy(new SmartParkingStrategy());
        Car car1 = new Car();
        Car car2 = new Car();
        //when
        parkingBoy.park(car1);
        Ticket ticket = parkingBoy.park(car2);
        //then
        assertEquals(car2, parkingLot2.fetch(ticket));

    }

    @Test
    void should_return_parkinglot2_ticket_when_fetch_twice_given_lot1_park1car_lot2_park1car() throws Exception {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(Arrays.asList(parkingLot1, parkingLot2));
        parkingBoy.setParkingStrategy(new SmartParkingStrategy());
        Car car1 = new Car();
        Car car2 = new Car();
        //when
        Ticket ticket1 = parkingBoy.park(car1);
        Ticket ticket2 = parkingBoy.park(car2);
        //then
        assertEquals(car1, parkingLot1.fetch(ticket1));
        assertEquals(car2, parkingLot2.fetch(ticket2));

    }

    @Test
    void should_throw_UnrecognizedTicketException_ticket_when_fetch_given_unrecognized_ticket() throws Exception {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();

        ParkingBoy parkingBoy = new ParkingBoy(Arrays.asList(parkingLot1, parkingLot2));
        parkingBoy.setParkingStrategy(new SmartParkingStrategy());
        Ticket ticket = new Ticket();

        //when

        //then
        assertThrows(UnrecognizedTicketException.class, () -> {
            parkingBoy.fetch(ticket);//could also test message
        });
    }

    @Test
    void should_throw_UnrecognizedTicketException_ticket_when_fetch_given_unrecognized_ticket_2lots() throws Exception {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();

        ParkingBoy parkingBoy = new ParkingBoy(Arrays.asList(parkingLot1, parkingLot2));
        parkingBoy.setParkingStrategy(new SmartParkingStrategy());
        Car car = new Car();
        Ticket ticket = parkingBoy.park(car);
        parkingBoy.fetch(ticket);

        //when

        //then
        assertThrows(UnrecognizedTicketException.class, () -> {
            parkingBoy.fetch(ticket);//could also test message
        });
    }

    @Test
    void should_throw_NoAvailablePositionException_ticket_when_fetch_given_2_full_lots() throws Exception {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);

        ParkingBoy parkingBoy = new ParkingBoy(Arrays.asList(parkingLot1, parkingLot2));
        parkingBoy.setParkingStrategy(new SmartParkingStrategy());
        parkingBoy.park(new Car());
        parkingBoy.park(new Car());

        //when

        //then
        assertThrows(NoAvailablePositionException.class, () -> {
            parkingBoy.park(new Car());//could also test message
        });
    }

    @Test
    void should_return_ticket_when_fetch_given_lot1_2Car_lot2_1Car() throws Exception {
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();

        ParkingBoy parkingBoy = new ParkingBoy(Arrays.asList(parkingLot1, parkingLot2));
        parkingBoy.setParkingStrategy(new SmartParkingStrategy());
        for (int i = 0; i < 3; i++) {
            parkingBoy.park(new Car());
        }
        Car fourthCar = new Car();
        Ticket ticket = parkingBoy.park(fourthCar);

        assertEquals(fourthCar, parkingLot2.fetch(ticket));
    }
}
