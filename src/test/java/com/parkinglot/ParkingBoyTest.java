package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingBoyTest {
    @Test
    void should_return_ticket_when_park_given_parkingLot_parkingBoy() throws Exception {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car car = new Car();

        //when
        Ticket actualTicket = parkingBoy.park(car);

        //then
        assertNotNull(actualTicket);
    }
    @Test
    void should_return_parked_car_when_fetch_given_ticket_parking_boy() throws Exception {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);

        Car car = new Car();
        Ticket actualTicket = parkingBoy.park(car);
        //when
        Car actualCar = parkingBoy.fetch(actualTicket);

        //then
        assertEquals(car, actualCar);
    }

    @Test
    void should_return_2_corresponding_car_when_fetch_2_times_given_2_tickets_parking_boy() throws Exception {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        Car car2 = new Car();

        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Ticket ticket = parkingBoy.park(car);
        Ticket ticket2 = parkingBoy.park(car2);
        //when
        Car actualCar = parkingBoy.fetch(ticket);
        Car actualCar2 = parkingBoy.fetch(ticket2);

        //then
        assertEquals(car, actualCar);
        assertEquals(car2, actualCar2);
    }

    @Test
    void should_return_UnrecognizedTicketException_when_fetch_given_wrong_ticket_parking_boy(){
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        Ticket wrongTicket = new Ticket();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        //when


        //then
        assertThrows(UnrecognizedTicketException.class, ()->{
            parkingBoy.fetch(wrongTicket);
        });
    }

    @Test
    void should_return_UnrecognizedTicketException_when_fetch_given_used_ticket() throws Exception {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();

        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Ticket usedTicket = parkingBoy.park(car);
        parkingBoy.fetch(usedTicket);


        //when
        //then
        assertThrows(UnrecognizedTicketException.class, ()->{
            parkingBoy.fetch(usedTicket);//could also test message
        });
    }

    @Test
    void should_return_NoAvailablePositionException_when_park_given_lot_full() throws Exception {
        ParkingLot parkingLot = new ParkingLot(2);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        parkingBoy.park(new Car());
        parkingBoy.park(new Car());

        //when
        //then
        //No available position.
        assertThrows(NoAvailablePositionException.class, ()->{
            parkingBoy.park(new Car());//could also test message
        });
    }
}
