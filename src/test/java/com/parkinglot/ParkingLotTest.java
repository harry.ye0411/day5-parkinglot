package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {
    @Test
    void should_return_ticket_when_park_given_parkingLot_car(){
        //given
        ParkingLot parkingLot = new ParkingLot(2);
        Car car = new Car();

        //when
        Ticket actualTicket = parkingLot.park(car);

        //then
        assertNotNull(actualTicket);
    }

    @Test
    void should_return_parked_car_when_fetch_given_ticket(){
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        Ticket ticket = parkingLot.park(car);
        //when
        Car actualCar = parkingLot.fetch(ticket);

        //then
        assertEquals(car, actualCar);
    }
    @Test
    void should_return_2_corresponding_car_when_fetch_2_times_given_2_tickets(){
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        Car car2 = new Car();
        Ticket ticket = parkingLot.park(car);
        Ticket ticket2 = parkingLot.park(car2);
        //when
        Car actualCar = parkingLot.fetch(ticket);
        Car actualCar2 = parkingLot.fetch(ticket2);

        //then
        assertEquals(car, actualCar);
        assertEquals(car2, actualCar2);
    }

    @Test
    void should_return_UnrecognizedTicketException_when_fetch_given_wrong_ticket(){
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        Ticket wrongTicket = new Ticket();
        //when


        //then
        assertThrows(UnrecognizedTicketException.class, ()->{
            parkingLot.fetch(wrongTicket);
        });
    }

    @Test
    void should_return_UnrecognizedTicketException_when_fetch_given_used_ticket(){
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        Ticket usedTicket = parkingLot.park(car);;

        //when
        parkingLot.fetch(usedTicket);
        //then
        assertThrows(UnrecognizedTicketException.class, ()->{
            parkingLot.fetch(usedTicket);//could also test message
        });

    }

    @Test
    void should_return_NoAvailablePositionException_when_park_given_lot_full(){
        ParkingLot parkingLot = new ParkingLot(2);
        parkingLot.park(new Car());
        parkingLot.park(new Car());
        //when
        //then
        //No available position.
        assertThrows(NoAvailablePositionException.class, ()->{
            parkingLot.park(new Car());//could also test message
        });
    }
}
