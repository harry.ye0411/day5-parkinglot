package com.parkinglot;

import java.util.List;
import java.util.Optional;

public class SuperParkingStrategy implements ParkingStrategy{
    @Override
    public Ticket park(Car car, List<ParkingLot> parkingLots) {
        Optional<ParkingLot> parkingLotOptional = parkingLots.stream().max((lot1, lot2) -> {
            Double lot1AvailablePositionRate = lot1.getAvailablePositionRate();
            Double lot2AvailablePositionRate = lot2.getAvailablePositionRate();
            return lot1AvailablePositionRate.compareTo(lot2AvailablePositionRate);
        });
        return parkingLotOptional.map(parkingLot -> parkingLot.park(car)).orElse(null);
    }
}
