package com.parkinglot;

import java.util.List;

public class ManagerParkingStrategy implements ParkingStrategy{
    public List<ParkingBoy> parkingBoys;

    public ManagerParkingStrategy(List<ParkingBoy> parkingBoys) {
        this.parkingBoys = parkingBoys;
    }

    @Override
    public Ticket park(Car car, List<ParkingLot> parkingLots) throws Exception {
        Exception lastException = null;
        for (ParkingBoy parkingBoy: parkingBoys) {
            try{
                return parkingBoy.park(car);
            } catch (Exception e) {
                lastException = e;
            }
        }
        assert lastException != null;
        throw lastException;
    }
}
