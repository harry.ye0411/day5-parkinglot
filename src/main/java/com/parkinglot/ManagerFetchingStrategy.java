package com.parkinglot;

import java.util.List;

public class ManagerFetchingStrategy implements FetchingStrategy{

    public List<ParkingBoy> parkingBoys;

    public ManagerFetchingStrategy(List<ParkingBoy> parkingBoys) {
        this.parkingBoys = parkingBoys;
    }
    @Override
    public Car fetch(Ticket ticket, List<ParkingLot> parkingLots) throws Exception {
        Exception lastException = null;
        for (ParkingBoy parkingBoy: parkingBoys) {
            try{
                return parkingBoy.fetch(ticket);
            } catch (Exception e) {
                lastException = e;
            }
        }
        assert lastException != null;
        throw lastException;
    }
}
