package com.parkinglot;

import java.util.HashMap;

public class ParkingLot {

    private final int maxCapacity;
    private final HashMap<Ticket, Car> ticketCar = new HashMap<>();

    public ParkingLot(int i) {
        this.maxCapacity = i;
    }

    public ParkingLot() {
        this.maxCapacity = 10;
    }

    public Ticket park(Car car) throws NoAvailablePositionException {
        if (isFull()) {
            throw new NoAvailablePositionException("No available position.");
        }
        Ticket newTicket = new Ticket();
        ticketCar.put(newTicket, car);
        return newTicket;
    }

    public Car fetch(Ticket ticket) throws UnrecognizedTicketException {
        if (!ticketCar.containsKey(ticket)) {
            throw new UnrecognizedTicketException("UnrecognizedTicket");
        }
        return ticketCar.remove(ticket);
    }

    public boolean isFull() {
        return ticketCar.size() == maxCapacity;
    }

    public int getAvailableCapacity() {
        return maxCapacity - ticketCar.size();
    }

    public double getAvailablePositionRate() {
        return (double) getAvailableCapacity() / this.maxCapacity;
    }
}
