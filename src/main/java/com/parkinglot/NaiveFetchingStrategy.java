package com.parkinglot;

import java.util.List;

public class NaiveFetchingStrategy implements FetchingStrategy{
    @Override
    public Car fetch(Ticket ticket, List<ParkingLot> parkingLots) throws Exception {
        Exception lastException = null;
        for (ParkingLot parkinglot: parkingLots) {
            try{
                return parkinglot.fetch(ticket);
            } catch (Exception e) {
                lastException = e;
            }
        }
        assert lastException != null;
        throw lastException;
    }
}
