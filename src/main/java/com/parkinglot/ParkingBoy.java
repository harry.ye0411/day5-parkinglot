package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class ParkingBoy {
    public List<ParkingLot> parkingLots;
    public ParkingStrategy parkingStrategy;
    public FetchingStrategy fetchingStrategy;

    public ParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
        this.parkingStrategy = new NaiveParkingStrategy();
        this.fetchingStrategy = new NaiveFetchingStrategy();
    }

    public ParkingBoy(ParkingLot parkingLot) {
        this.parkingLots = new ArrayList<>();
        this.parkingLots.add(parkingLot);
        this.parkingStrategy = new NaiveParkingStrategy();
        this.fetchingStrategy = new NaiveFetchingStrategy();
    }

    public void setFetchingStrategy(FetchingStrategy fetchingStrategy) {
        this.fetchingStrategy = fetchingStrategy;
    }

    public void setParkingStrategy(ParkingStrategy parkingStrategy) {
        this.parkingStrategy = parkingStrategy;
    }

    public Ticket park(Car car)  throws Exception {
        return parkingStrategy.park(car, parkingLots);
    }

    public Car fetch(Ticket ticket) throws Exception{
        return fetchingStrategy.fetch(ticket, parkingLots);
    }
}
