package com.parkinglot;

import java.util.List;

public class NaiveParkingStrategy implements ParkingStrategy{

    @Override
    public Ticket park(Car car, List<ParkingLot> parkingLots) throws Exception {
        Exception lastException = null;
        for (ParkingLot parkingLot : parkingLots) {
            try {
                return parkingLot.park(car);
            } catch (Exception e) {
                lastException = e;
            }
        }
        assert lastException != null;
        throw lastException;
    }
}
