package com.parkinglot;

import java.util.List;
import java.util.Optional;

public class SmartParkingStrategy implements ParkingStrategy{
    @Override
    public Ticket park(Car car, List<ParkingLot> parkingLots) {
        Optional<ParkingLot> parkingLotOptional = parkingLots.stream().max((lot1, lot2) -> {
            Integer lot1AvailableCapacity = lot1.getAvailableCapacity();
            Integer lot2AvailableCapacity = lot2.getAvailableCapacity();
            return lot1AvailableCapacity.compareTo(lot2AvailableCapacity);
        });
        return parkingLotOptional.map(parkingLot -> parkingLot.park(car)).orElse(null);
    }
}
