package com.parkinglot;

import java.util.List;

public interface FetchingStrategy {
    Car fetch(Ticket ticket, List<ParkingLot> parkingLots) throws Exception;
}
